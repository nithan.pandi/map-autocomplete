import logo from './logo.svg';
import './App.css';
import Map from './Map'
import { locations } from './public/locations'
import { useState } from 'react'
import { Input } from 'antd';


function App() {
  const [filterLocation, setFilterLocation] = useState([])
  const [lat, setLat] = useState(null)
  const [long, setLong] = useState(null)
  
  const setLocation = (e) => {
    const filteredData = filterByLocationSubstring(locations, e.target.value);
    setFilterLocation(filteredData)
  }

  function filterByLocationSubstring(arr, substring) {
    return arr.filter(obj => obj.name?.toLowerCase().includes(substring.toLowerCase()));
  }

  const setLatLong = (location) => {
    setLat(location.latitude)
    setLong(location.longitude)
  }


  return (
    <div className="App">
     <Input placeholder="Search locations" onChange={setLocation} />

      {
        filterLocation.map(l => {
          return (
            <p onClick={() => setLatLong(l)}>{l.name ?? ''}</p>
          )
        })
      }

      <>
        {
          lat & long ? <Map latitude={lat} longitude={long} /> : <></>
        }
      </>
    </div>

    // Commenting for google auto complete with google maps, since not using api keys
    // <div>
    //   <MapWithSearchBox />
    // </div>
  );
}

export default App;
