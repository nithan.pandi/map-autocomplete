import React from 'react';
import MapWithMarker from './MapWithMarker';

const Map = ({ latitude, longitude }) => {
    return (
        <div>
            <MapWithMarker latitude={latitude} longitude={longitude} />
        </div>
    );
};

export default Map;
