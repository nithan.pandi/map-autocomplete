
export const locations = [
    {
        "name": "Andhra Pradesh",
        "latitude": 15.9129,
        "longitude": 79.74
    },
    {
        "name": "Arunachal Pradesh",
        "latitude": 27.0817,
        "longitude": 93.6166
    },
    {
        "name": "Assam",
        "latitude": 26.2006,
        "longitude": 92.9376
    },
    {
        "name": "Bihar",
        "latitude": 25.0961,
        "longitude": 85.3131
    },
    {
        "name": "Chhattisgarh",
        "latitude": 21.2787,
        "longitude": 81.8661
    },
    {
        "name": "Goa",
        "latitude": 15.2993,
        "longitude": 74.124
    },
    {
        "name": "Gujarat",
        "latitude": 22.2587,
        "longitude": 71.1924
    },
    {
        "name": "Haryana",
        "latitude": 29.0588,
        "longitude": 76.0856
    },
    {
        "name": "Himachal Pradesh",
        "latitude": 31.1048,
        "longitude": 77.1734
    },
    {
        "name": "Jharkhand",
        "latitude": 23.6102,
        "longitude": 85.2799
    },
    {
        "name": "Karnataka",
        "latitude": 15.3173,
        "longitude": 75.7139
    },
    {
        "name": "Kerala",
        "latitude": 10.8505,
        "longitude": 76.2711
    },
    {
        "name": "Madhya Pradesh",
        "latitude": 22.9734,
        "longitude": 78.6569
    },
    {
        "name": "Maharashtra",
        "latitude": 19.7515,
        "longitude": 75.7139
    },
    {
        "name": "Manipur",
        "latitude": 24.6637,
        "longitude": 93.9063
    },
    {
        "name": "Meghalaya",
        "latitude": 25.467,
        "longitude": 91.3662
    },
    {
        "name": "Mizoram",
        "latitude": 23.1645,
        "longitude": 92.9376
    },
    {
        "name": "Nagaland",
        "latitude": 26.1584,
        "longitude": 94.5624
    },
    {
        "name": "Odisha",
        "latitude": 20.9517,
        "longitude": 85.0985
    },
    {
        "name": "Punjab",
        "latitude": 31.1471,
        "longitude": 75.3412
    },
    {
        "name": "Rajasthan",
        "latitude": 27.0238,
        "longitude": 74.2179
    },
    {
        "name": "Sikkim",
        "latitude": 27.533,
        "longitude": 88.5122
    },
    {
        "name": "Tamil Nadu",
        "latitude": 11.1271,
        "longitude": 78.6569
    },
    {
        "name": "Telangana",
        "latitude": 17.1232,
        "longitude": 79.2088
    },
    {
        "name": "Tripura",
        "latitude": 23.9408,
        "longitude": 91.9882
    },
    {
        "name": "Uttar Pradesh",
        "latitude": 26.8467,
        "longitude": 80.9462
    },
    {
        "name": "Uttarakhand",
        "latitude": 30.0668,
        "longitude": 79.0193
    },
    {
        "name": "West Bengal",
        "latitude": 22.9868,
        "longitude": 87.855
    }
]

