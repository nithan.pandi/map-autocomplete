import React, { useState } from 'react';
import Autosuggest from 'react-autosuggest';
import { locations } from './public/locations'

const AutocompleteInput = ({ suggestions, onSuggestionSelected }) => {
    const [value, setValue] = useState('');
    const [suggestionsList, setSuggestionsList] = useState([]);

    const onChange = (event, { newValue }) => {
        setValue(newValue);
    };

    const getSuggestions = (value) => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;
        return inputLength === 0 ? [] : suggestions.filter(
            suggestion => suggestion.toLowerCase().slice(0, inputLength) === inputValue
        );
    };

    const onSuggestionsFetchRequested = ({ value }) => {
        setSuggestionsList(getSuggestions(value));
    };

    const onSuggestionsClearRequested = () => {
        setSuggestionsList([]);
    };

    const getSuggestionValue = (suggestion) => {
        return suggestion;
    };

    const renderSuggestion = (suggestion) => {
        return (
            <div>
                {suggestion}
            </div>
        );
    };

    const inputProps = {
        placeholder: 'Type a name',
        value,
        onChange: onChange
    };

    return (
        <Autosuggest
            suggestions={suggestionsList}
            onSuggestionsFetchRequested={onSuggestionsFetchRequested}
            onSuggestionsClearRequested={onSuggestionsClearRequested}
            getSuggestionValue={getSuggestionValue}
            renderSuggestion={renderSuggestion}
            onSuggestionSelected={(event, { suggestionValue }) => {
                onSuggestionSelected(suggestionValue);
            }}
            inputProps={inputProps}
        />
    );
};

export default AutocompleteInput;
